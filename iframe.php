<html>
<head>
  <meta charset="utf-8">
  <!-- http://garden.zendesk.com -->
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
  <link href="https://cdn.jsdelivr.net/bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet">
  <link href="main.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">

  <script type="text/javascript">
  function showOrderItems(orderNumber){
	if ($('#'+orderNumber).is(':visible')){
		$('#'+orderNumber).hide();
	}
	else {
		$('#'+orderNumber).show();
	}
}
  
  
  </script>
 </head>
  
<body>
  <!-- https://github.com/zendesk/zendesk_app_framework_sdk -->
  <script src="https://cdn.jsdelivr.net/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://assets.zendesk.com/apps/sdk/2.0/zaf_sdk.js"></script>
   
	
	
  <div class="container"  >
   
    <table class="table">
         <thead class="thead-dark">
		<tr style="color:green; text-align:center; font-family: 'Roboto Condensed','sans-serif'; font-size:20px ">
            <th scope="col"></th>
			<th scope="col">Order</th>
            <th scope="col" width="150">Created At</th>
			<th scope="col" width="150">Total Amount</th>
			<th scope="col" width="200">Payment Method</th>
			<th scope="col">Status</th>
			<th scope="col">Email</th>
		</tr>
    </thead>
<?php 


$dated=new DateTime(); //this returns the current date time
$newdate = $dated->modify('-1 months');
$result = $newdate->format('Y-m-d');

$krr = explode('-',$result);
$result = implode("",$krr);
$url = "https://b6680cafb30fdb939342b2bf59249617:0782bcc5bba458507a575d6f952732f8@test-12345678900000000000323.myshopify.com/admin/orders.json?created_at_min=".$result;
$start = curl_init();
curl_setopt($start, CURLOPT_URL, $url);
curl_setopt($start, CURLOPT_RETURNTRANSFER, true);
$responseJson = curl_exec($start);
curl_close($start);
$response = json_decode($responseJson,true);
 
?>
       <?php foreach($response as $key):
				foreach($key as $k):
	   ?>
			
    	<tr onclick="showOrderItems(<?php echo $k["order_number"]?>);">
			<td style="width:2pc;"><img src="Image/download.png" height="15" width="15"></td>
			<td><?php echo $k["order_number"]. "   " ?></td>
			<td><?php $time = $k["created_at"];$date = new DateTime($time); echo $date->format('H:i:s d-m-y');  ?></td>
			<td><?php echo $k["total_price"]. "   " .$k["currency"] ?></td>
			<td><?php echo $k["gateway"]. "   "?></td>
			<td><?php echo $k["financial_status"]. "   " ?></td>
			<td><?php echo $k["email"]. "    " ?></td>
		</tr>
		<tr id="<?php echo $k["order_number"]?>" style="display:none;"> 
			<td style="width:2pc;border:0px;">&nbsp;</td>
			<td colspan="6" style="border:1px ;">
				<table class="nested" style="border:1px ;background-color:white;">
						<tr style="color:green; text-align:center; font-family: 'Roboto Condensed','sans-serif'; font-size:20px">
						<td width="200">Item     </td>
						<td width="100">Size      </td>
						<td width="100">Price     </td>
						<td width="100">Quantity  </td>
						</tr>
						<?php foreach ($k["line_items"] as $data): ?>
						<tr>
							<td><?php echo $data["title"]. "   " ?></td>
							<td><?php echo $data["variant_title"]. "    " ?></td>
							<td><?php echo $data["price"]."   ".$k["currency"]."   " ?></td>
							<td><?php echo $data["quantity"]. "   " ?></td>
						</tr>
						<?php endforeach; ?>
				</table>
				
			</td>
		</tr>		
	   <?php endforeach; endforeach; ?>
</table>
</div>
</body>
</html>
